import os
from string import Template

template = Template("".join(open("template.html").readlines()))
data = {
    '01.html': '0003',
    '02.html': '0004',
    '03.html': '0012',
    '04.html': '0018',
    '05.html': '0024',
    '06.html': '0025',
    '07.html': '0028',
    '08.html': '0033',
    '09.html': '0046',
    '10.html': '0060',
    '11.html': '0053',
    '12.html': '0068',
    '13.html': '0070',
    '14.html': '0071',
    '15.html': '0073',
    '16.html': '0076',
    '17_10.html': '0079',
    '17_20.html': '0080',
    '17_40.html': '0081',
    '17_80.html': '0082',
    '18.html': '0170',
    '19.html': '0171',
    '20.html': '0172',
    '21.html': '0181',
    '22.html': '0182',
    '23.html': '0183',
    '24.html': '0184',
    '25.html': '0185',
    '26.html': '0186',
    '27.html': '0187',
    '28.html': '0188',
}

for viewer, image in data.items():
    html = template.substitute({'file': f'DJI_{image}_pano.jpg'})
    with open(os.path.join('.public', viewer), 'w') as sink:
        sink.write(html)
